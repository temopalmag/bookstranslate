# PARTE 1. Visión General

## Capítulo 1. Introducción a Kubernetes

Este capítulo abarca

* Entendiendo cómo el desarrollo de software y el despliegue ha cambiado en los años recientes.
* Aislamiento de aplicaciones y reducción de diferencias de ambiente usando contenedores.
* Entendiendo cómo los contenedores y Docker son usados por Kubernetes.
* Haciendo el trabajo de los desarrolladores y sysadmins más fácil con Kubernetes.

Hace años, la mayoría de las aplicaciones de software eran grandes monolitos, corriendo ya sea como un único proceso o como un pequeño número de procesos esparcidos en un puñado de servidores. Estos sistemas heredados están aún muy extendidos hoy. Tienen ciclos de liberación lentos y son actualizados relativamente con poca frecuencia. Al final de cada ciclo de liberación, los desarrolladores empaquetan todo el sistema y se lo entregan al equipo de operaciones, quienes entonces lo despliegan y monitorean. En caso de fallos de hardware, el equipo de operaciones lo migra manualmente a los servidores saludables restantes.

Hoy, estos grandes monolitos de aplicaciones heredadas están siendo lentamente divididos en componentes más pequeños e independientes, llamados microservicios. Ya que los microservicios son desacoplados uno del otro, pueden ser desarrollados, desplegados, actualizados y escalados individualmente. Esto les permite cambiar componentes rápidamente y tan frecuente como sea necesario para mantenerse al día con los cambiantes requerimientos de negocio de hoy.

Pero con un mayor número de componentes desplegables y centros de datos cada vez más grandes, se vuelve cada vez más difícil de configurar, administrar y mantener todo el sistema funcionando sin problemas. Es mucho más difícil averiguar dónde colocar cada uno de esos componentes para lograr una alta utilización de los recursos y por lo tanto, mantener bajos los costos del hardware. Hacer todo esto manualmente es un trabajo difícil. Necesitamos automatización, la cual incluye programación automática de esos componentes en nuestros servidores, configuración automática, supervisión, y manejo de errores. Acá es dónde Kubernetes entra.

